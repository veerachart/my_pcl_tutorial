#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
// PCL specific includes
#include <pcl/ros/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl_ros/point_cloud.h>
#include <geometry_msgs/Vector3.h>

ros::Publisher plane_pub;
ros::Publisher coeff_pub;
ros::Publisher vector_pub;
geometry_msgs::Vector3 vector_msg;

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud
  pcl::PointCloud<pcl::PointXYZ> cloud, cloud_p, cloud_f;
  sensor_msgs::PointCloud2 cloud_filtered;

  // Perform the actual filtering
  pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
  sor.setInputCloud (input);
  sor.setLeafSize (0.01, 0.01, 0.01);
  sor.filter (cloud_filtered);
  pcl::fromROSMsg (cloud_filtered, cloud);

  pcl::ModelCoefficients coefficients;
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  // Create the filtering object
  pcl::ExtractIndices<pcl::PointXYZ> extract;

  // Segment the largest planar component from the remaining cloud
  seg.setInputCloud (cloud.makeShared());
  seg.segment (*inliers, coefficients);
  if (inliers->indices.size () == 0)
  {
    std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
    return;
  }

  // Extract the inliers
  extract.setInputCloud (cloud.makeShared());
  extract.setIndices (inliers);
  extract.setNegative (false);
  extract.filter (cloud_p);

  // Create the filtering object
  extract.setNegative (true);
  extract.filter (cloud_f);
  cloud.swap (cloud_f);

  plane_pub.publish (cloud_p);
  vector_msg.x = coefficients.values[0];
  vector_msg.y = coefficients.values[1];
  vector_msg.z = coefficients.values[2];
  coeff_pub.publish (coefficients);
  vector_pub.publish (vector_msg);
}

int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "my_pcl_tutorial");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("input", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud
  plane_pub = nh.advertise<pcl::PointCloud<pcl::PointXYZ> > ("plane", 1);
  // Create a ROS publisher for the output coefficients
  coeff_pub = nh.advertise<pcl::ModelCoefficients> ("coefficients", 1);
  // Create a ROS publisher for the output coefficients as Vector3
  vector_pub = nh.advertise<geometry_msgs::Vector3> ("normal", 1);

  // Spin
  ros::spin ();
}
